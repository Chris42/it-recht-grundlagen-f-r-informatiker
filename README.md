# README #


### Wofür ist dieses Repository gedacht? ###

* Zum Erstellen der Ausarbeitung und des Vortrags für das Modul IT-Recht Grundlagen für Informatiker

### Was wird benötigt? ###

* Ein Programm zum bearbeiten von .txt-Dateien. Empfohlen wird ein LaTex-Editor.
* Ein Programm zum Anzeigen von PDF-Dateinen.
* In Programm für die Verwaltung des GIT-Repositories. Empfohlen wird SourceTree.



### Urheber und Autoren  ###

* Christian Riest 
* Sascha Threbank